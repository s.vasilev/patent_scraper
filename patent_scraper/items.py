# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class PatentItem(scrapy.Item):
    url = scrapy.Field()
    title = scrapy.Field()
    claims = scrapy.Field()
    ipcs = scrapy.Field()
    abstract = scrapy.Field()
    applicants = scrapy.Field()
    inventors = scrapy.Field()
    number = scrapy.Field()
    description = scrapy.Field()
