import scrapy
import re

from ..items import PatentItem
from scrapy.http import Request

from datetime import date
from dateutil.relativedelta import relativedelta
from typing import List, Union


class EspacenetSpider(scrapy.Spider):
    name = 'espacenet'
    allowed_domains = ['espacenet.com']
    start_urls = ['http://espacenet.com/']
    base_url = "https://ru.espacenet.com"

    start_date = date(2000, 1, 1)
    end_date = date(2022, 7, 1)
    CLEANER = re.compile('<.*?>')

    TARGET_IPC_CODE = "B02C"
    TARGET_TITLE = "щековая дробилка"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def start_requests(self):
        url = self.base_url + '/data/searchResults'
        base_params = {
            "DB": "ru.espacenet.com",
            "IC": self.TARGET_IPC_CODE,
            "page": "0",
            "TI": self.TARGET_TITLE,
            "PD": ""
        }
        requests = []
        daterange = self.daterange_by_month(
            self.start_date,
            self.end_date
        )
        for date_gap in daterange:
            params = base_params.copy()
            params["PD"] = date_gap
            request = scrapy.FormRequest(
                url=url,
                method='GET',
                formdata=params,
                callback=self.parse_pagination
            )
            requests.append(request)

        return requests

    def parse_pagination(self, response):
        doc_url_xpath = "//a[@class='publicationLinkClass']/@href"
        page_urls = response.xpath(doc_url_xpath).extract()
        for url_part in page_urls:
            page_url = self.base_url + '/data' + url_part
            yield Request(url=page_url, method='GET', callback=self.parse_page)

        next_page_url = self._parse_next_page(response)
        if next_page_url:
            request = Request(
                url=next_page_url,
                method='GET',
                callback=self.parse_pagination
            )
            yield request

    def _parse_next_page(self, response) -> Union[str, None]:
        next_page_xpath = "//a[@id='nextPageLinkTop']/@href"
        next_page_raw = response.xpath(next_page_xpath).extract_first()
        next_page_url = self.base_url + '/data/' + next_page_raw if next_page_raw else None
        return next_page_url

    def parse_page(self, response):
        claims_url_xpath = "//a[@accesskey='3']/@href"
        claims_url_raw = response.xpath(claims_url_xpath).extract_first()
        if claims_url_raw:
            claims_url = self.base_url + '/data' + claims_url_raw
        else:
            return

        data = {
            "url": response.request.url,
            "title": self._parse_title(response),
            "inventors": self._parse_inventors(response),
            "applicants": self._parse_applicants(response),
            "number": self._parse_app_number(response),
            "ipcs": self._parse_ipc(response),
            "abstract": self._parse_abstract(response),
        }

        request = Request(
            url=claims_url,
            method='GET',
            callback=self.parse_claims
        )
        request.meta['data'] = data
        yield request

    def parse_claims(self, response):
        claim_xpath = "//div[@id='claims']/p[@lang='ru']"
        claims_raw = response.xpath(claim_xpath).extract()
        if not claims_raw:
            return

        # self.logger.info(claims_raw)
        claims = re.split("<br>\n?<br>\s+", str(claims_raw[0]))
        claims = [self._clean_html(claim) for claim in claims]
        claims = list(filter(lambda x: x, claims))

        if not claims:
            return

        meta = response.meta.get('data', {})
        meta['claims'] = claims
        # self.logger.info(claims)

        description_url_xpath = "//a[@accesskey='2']/@href"
        description_url_raw = response.xpath(description_url_xpath).extract_first()
        if description_url_raw:
            description_url = self.base_url + '/data' + description_url_raw
            request = Request(
                url=description_url,
                method='GET',
                callback=self.parse_description
            )
            request.meta['data'] = meta
            yield request
        else:
            item = PatentItem(**meta)
            yield item

    def parse_description(self, response):
        meta = response.meta.get('data', {})
        meta['description'] = self._parse_description(response)

        item = PatentItem(**meta)
        yield item

    def _parse_title(self, response) -> str:
        title_xpath = "//div[@id='body']/preceding-sibling::h3"
        title_raw = response.xpath(title_xpath).extract_first()
        title = self._clean_html(title_raw) if title_raw else ""
        return title

    def _parse_description(self, response) -> List:
        description_xpath = "//div[@id='description']/p[@lang='ru']/text()"
        description_raw = response.xpath(description_xpath).extract()
        description = self._clean_list(description_raw) if description_raw else []
        return description

    def _parse_inventors(self, response) -> List:
        inventors_xpath = "//span[@id='inventors']/text()"
        inventors_raw = response.xpath(inventors_xpath).extract_first()
        if inventors_raw:
            inventors_raw = self._clean_html(inventors_raw)
            inventors = [inv.strip().rstrip(',') for inv in inventors_raw.split(";")]
            inventors = self._filter_empty(inventors)
        else:
            inventors = []
        return inventors

    def _parse_applicants(self, response) -> str:
        applicants_xpath = "//span[@id='applicants']/text()"
        applicants_raw = response.xpath(applicants_xpath).extract_first()
        if applicants_raw:
            applicants = self._clean_html(applicants_raw)
        else:
            applicants = ""
        return applicants

    def _parse_ipc(self, response) -> List:
        ipc_xpath = "//a[@class='ipc classTT']/text()"
        ipcs_raw = response.xpath(ipc_xpath).extract()
        ipcs = self._clean_list(ipcs_raw) if ipcs_raw else []
        return ipcs

    def _parse_app_number(self, response) -> str:
        app_number_xpath = "//th[contains(text(), 'Номер заявки:')]/following-sibling::td[1]/text()"
        app_number_raw = response.xpath(app_number_xpath).extract_first()
        app_number = self._clean_html(app_number_raw) if app_number_raw else ""
        return app_number

    def _parse_abstract(self, response) -> List:
        abstract_xpath = "//p[@lang='ol' and @class='printAbstract']/text()"
        abstract_raw = response.xpath(abstract_xpath).extract()
        abstract = self._clean_list(abstract_raw) if abstract_raw else []
        return abstract

    def _clean_list(self, content: List) -> List:
        content = [self._clean_html(c) for c in content]
        content = self._filter_empty(content)
        return content

    def _filter_empty(self, content: List) -> List:
        return list(filter(lambda x: x, content))

    def _clean_html(self, raw_html: str) -> str:
        cleaned_text = re.sub(self.CLEANER, '', raw_html)
        cleaned_text = cleaned_text.replace('&nbsp;', '').strip()
        return cleaned_text

    def daterange_by_month(self, start_date, end_date) -> str:
        """Генератор диапазонов дат для поискового фильтра"""
        delta = relativedelta(months=+1)
        template = "{}-{}"
        while start_date < end_date:
            next_date = start_date + delta
            item = template.format(
                self._format_date(start_date),
                self._format_date(next_date)
            )
            yield item
            start_date = next_date

    def _format_date(self, target_date: date) -> str:
        return target_date.strftime("%Y%m%d")
