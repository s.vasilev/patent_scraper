# patent_scraper

Парсер патентных БД на основе [Scrapy](https://scrapy.org/)

Пример запуска:
```bash
$ scrapy crawl espacenet -o ./output/patents.json
```